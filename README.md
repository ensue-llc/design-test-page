# Test for User Interface/Experience Designer
#### Ensue LLC, Irving, TX

## Description
* Page is native at 1080P resolution.
* All images used are provided as SVGs.
* Font used are different weights of Roboto Condensed. Please use Google Fonts.
* This project is for evaluation purpose only. All applicants are given the same task and provided code will not be used in any environment.


##### Goals - Composition to HTML/CSS
1. Desktop: The Page/contents of the page should scale proportionately BETWEEN the following resolutions without breaking the layout. This is the most important feature. ALL ELEMENTS OF THE PAGE SHOULD SCALE PROPORTIONATELY.
    - Minimum : iPad resolution (1024 x 768 effective px - Portrait + Landscape)
    - Native: 1080p (native)
    - Maximum: 4K resolution (3840 x 2160px)

2. Mobile : No composition provided intentionally. Use your best judgement to convert and style this page to be mobile compatible. Should be compatible with iPhone 6/7/8/+/X.

3. Do not build this on top of any existing CSS framework. Please write everything from scratch. Mixins allowed.

4. Less/SASS preferred over Vanilla CSS.

5. Should be consistent on the following browsers:
    - Chrome, Chromium, Firefox, Safari, Mobile Safari(iPad), Internet Explorer v11.x, Microsoft Edge v42.x

6. Should be delivered to us in a publicly accessible deployed URL + Source code via a hosted repository link. Do not email zip files/folders.

7. We will evaluate all aspects of this test project:
    - HTML Markup Structure, efficiency and validity
    - Stylesheet efficiency and organization
    - Code/Folder Structure
    - Delivery

8. Section 1 and Section 2 should be equal in height - regardless or window size and should consume half of the available vertical space each.

9. Section 1/2 and Section 3 should be equal in width - regardless of window size and should consume half of the available horizontal space each.

10. In Section 1 and Section 3, there will be more data than is visible. Vertical scrolling should be implemented.

12. Any data container can have more data than it fits, for example, in Section 1 ENS001, can be ENSUEDALLAS100001, but since there is only limited horizontal space, if the data is longer than the container width, truncate the element with ellipsis to show only characters that can fit eg: ENSUEDALL... 

13. Use of JS/jQuery not allowed.

14. +++ If able to add any additional visual enhancement without affecting the page-consumption time and existing layout.

##### Testing Procedure
1. Does the styling match provided comps ? (Y/N)
2. Does the page match the comp at resolution 1080p ? (Y/N)
3. Do all the elements of the page scale with screen with ? (Y/N)
4. When additional content is addition in each section, does the layout maintain itself ? (Y/N)
5. Is there usage of a pre-existing grid/framework ? (Y/N)
6. Is the markup organized and structured properly ? (Y/N)
7. Is the stylesheet organized and structured properly ? (Y/N)
8. Did the user create a mobile stylesheet (with or w/o Media Query) ? (Y/N)
9. Mobile Styling Quality
10. Did the user add any pizzaz (enhancements) using own judgement ? (Y/N)
11. Did the user implement truncation in fields ? (Y/N)


###### Thank you for taking your precious time out and completing this test. You are one step closer to joining our team. HAPPY CODING